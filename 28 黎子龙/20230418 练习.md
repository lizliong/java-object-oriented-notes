L1

```java
import java.util.ArrayList;

public class L1 {
    /*
    创建一个存储字符串的集合，存储3个字符串元素，使用程序实现在控制台遍历该集合
    思路：
创建集合对象
往集合中添加字符串对象
遍历集合，首先要能够获取到集合中的每一个元素，这个通过get(int index)方法实现
遍历集合，其次要能够获取到集合的长度，这个通过size()方法实现
遍历集合的通用格式
     */
   public static void main(String[] args) {
        ArrayList<String> a = new ArrayList<>();
        a.add("123");
        a.add("456");
        a.add("789");

        for (int i = 0; i < a.size(); i++) {
           System.out.println(a.get(i));
       }
   }
}
```

L2

```java
public class Student {
    /*
    创建一个存储学生对象的集合，存储3个学生对象，使用程序实现在控制台遍历该集合
    思路：
定义学生类
创建集合对象
创建学生对象
添加学生对象到集合中
遍历集合，采用通用遍历格式实现
     */
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Student() {
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return name+" "+age;
    }
}
```



```java
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Student> stu = new ArrayList<>();
        stu.add(new Student("li",13));
        stu.add(new Student("zhao",14));
        stu.add(new Student("zhang",15));

        for (int i = 0; i < stu.size(); i++) {
            System.out.println(stu.get(i));
        }
    }
}
```



L3

```java
public class Student {
    /*
   创建一个存储学生对象的集合，存储3个学生对象，使用程序实现在控制台遍历该集合学生的姓名和年龄来自于键盘录入
        思路：
定义学生类，为了键盘录入数据方便，把学生类中的成员变量都定义为String类型
创建集合对象
键盘录入学生对象所需要的数据
创建学生对象，把键盘录入的数据赋值给学生对象的成员变量
往集合中添加学生对象
遍历集合，采用通用遍历格式实现
     */
    private String name;
    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Student() {
    }

    @Override
    public String toString() {
        return name+" "+age;
    }

    public Student(String name, String age) {
        this.name = name;
        this.age = age;
        
    }
}
```



```java
import java.util.ArrayList;
import java.util.Scanner;

public class Ax {
    public static void main(String[] args) {
        ArrayList<Student> stu = new ArrayList<>();

        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.print("请输入第"+(i+1)+"个学生的姓名：");
            String a = sc.next();
            System.out.print("请输入第"+(i+1)+"个学生的年龄：");
            String b = sc.next();
            stu.add(new Student(a,b));
        }
        for (int i = 0; i < 3; i++) {
            System.out.println(stu.get(i));
        }
    }
}
```



L4

```java
import java.util.ArrayList;

public class L4 {
    /*
    需求：创建一个存储String的集合，内部存储（test，张三，李四，test，test）字符串
删除所有的test字符串，删除后，将集合剩余元素打印在控制台
思路：
创建集合对象
调用add方法，添加字符串
遍历集合，取出每一个字符串元素
加入if判断，如果是test字符串，调用remove方法删除
打印集合元素

     */
    public static void main(String[] args) {
        ArrayList<String> a = new ArrayList<>();
        a.add("test");
        a.add("张三");
        a.add("李四");
        a.add("test");
        a.add("test");

        String b = "test";

        for (int i = 0; i < a.size(); i++) {
            if (a.get(i).equals(b)) {
                a.remove(i);
                i--;
            }
        }
        System.out.println(a);
    }
}
```



