1. 父类是：战斗机
   1. 属性：名称，颜色，载弹量，打击半径
   2. 功能：起飞，巡航，降落，雷达扫射，开火

2. 接口：对空，对地
3. 子类：
   1. 空空战斗机（只对空）
   2. 空地战斗机（能对空能对地）
   3. 轰炸机 （只能对地）

4. 测试类：
   1. 生成每一种战斗并属性赋值，并调用相关功能



```java

/*
1. 父类是：战斗机
   1. 属性：名称，颜色，载弹量，打击半径
   2. 功能：起飞，巡航，降落，雷达扫射，开火
 */
public class zdj {
    private String name;
    private String color;
    private int zaidanl;
    private double banjing;

    public void qifei(){
        System.out.println("能起飞");
    }

    public void xunhang(){
        System.out.println("能巡航");
    }

    public void jinagluo(){
        System.out.println("能降落");
    }

    public void saoshe(){
        System.out.println("能雷达扫射");
    }

    public void kaihuo(){
        System.out.println("能开火");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getZaidanl() {
        return zaidanl;
    }

    public void setZaidanl(int zaidanl) {
        this.zaidanl = zaidanl;
    }

    public double getBanjing() {
        return banjing;
    }

    public void setBanjing(double banjing) {
        this.banjing = banjing;
    }

    public zdj() {
    }

    public zdj(String name, String color, int zaidanl, double banjing) {
        this.name = name;
        this.color = color;
        this.zaidanl = zaidanl;
        this.banjing = banjing;
    }

    @Override
    public String toString() {
        return "zdj{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", zaidanl=" + zaidanl +
                ", banjing=" + banjing +
                '}';
    }
}
```



```java
/*
2. 接口：对地
 */
public interface duidi {
    void duidi();
}
```



```java
/*
2. 接口：对空
 */
public interface duikong {
    public abstract void duikong();
}
```



```java
/*
   1. 空空战斗机（只对空）
 */
public class kongkong extends zdj implements duikong{
    @Override
    public void duikong() {
        System.out.println(getName()+"能对空中目标进行打击");
    }

    public kongkong() {
    }

    public kongkong(String name, String color, int zaidanl, double banjing) {
        super(name, color, zaidanl, banjing);
    }
}
```



```java
/*
   2. 空地战斗机（能对空能对地）
 */
public class kongdi extends zdj implements duikong,duidi{
    @Override
    public void duidi() {
        System.out.println(getName()+"能对空中目标进行扫射");
    }

    @Override
    public void duikong() {
        System.out.println(getName()+"能对地面目标进行扫射");
    }

    public kongdi() {
    }

    public kongdi(String name, String color, int zaidanl, double banjing) {
        super(name, color, zaidanl, banjing);
    }
}
```



```java
/*
   3. 轰炸机 （只能对地）
 */
public class hongzha extends zdj implements duidi{
    @Override
    public void duidi() {
        System.out.println(getName()+"能对地面目标进行轰炸");
    }

    public hongzha() {
    }

    public hongzha(String name, String color, int zaidanl, double banjing) {
        super(name, color, zaidanl, banjing);
    }
}
```



```java
/*
4. 测试类：
 */
public class Test {
    public static void main(String[] args) {
        kongkong kk1 = new kongkong("对空1号","黑色",200,600);
        System.out.println(kk1);
        kk1.duikong();

        kongdi kd1 = new kongdi("空地1号","灰色",100,300);
        System.out.println(kd1);
        kd1.duikong();
        kd1.duidi();

        hongzha hz1 = new hongzha("轰炸1号","褐色",50,100);
        System.out.println(hz1);
        hz1.duidi();
    }
}
```

